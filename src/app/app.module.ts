import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { TopmenuComponent } from './components/topmenu/topmenu.component';
import { HomeComponent } from './components/home/home.component';
import { TabAComponent } from './components/tab-a/tab-a.component';
import { TabBComponent } from './components/tab-b/tab-b.component';
import { TabCComponent } from './components/tab-c/tab-c.component';
import { TabDComponent } from './components/tab-d/tab-d.component';

export const APP_ID = 'my-app';

@NgModule({
  declarations: [
    AppComponent,
    TopmenuComponent,
    TabAComponent,
    HomeComponent,
    TabBComponent,
    TabCComponent,
    TabDComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: APP_ID }),
	HttpModule,
    RouterModule,
    routing
  ],
  providers: [],
  entryComponents: [AppComponent],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
