import { Component, OnInit } from '@angular/core';
//import {CacheService, CacheStorageAbstract, CacheLocalStorage} from 'ng2-cache/ng2-cache';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-b',
  templateUrl: './tab-b.component.html',
  styleUrls: ['./tab-b.component.css'],
  providers: [
    //CacheService, {provide: CacheStorageAbstract, useClass:CacheLocalStorage}
  ]
})
export class TabBComponent implements OnInit {

  colHead = [];
  randomInt = [];

  constructor(
    //private _cacheService: CacheService,
    //private _ngCache: CacheService,
    router : Router
  ) {}

  ngOnInit() {
    /*this.colHead = [1,2,3,4,5,6,7,8,9,10];
    if(this._cacheService.exists('tab-b') === true){
      this.randomInt = this._ngCache.get('tab-b');
    }else{
      this.setRandomInt();
      if(this._cacheService.exists('tab-b') === false){
        this._ngCache.set('tab-b',this.randomInt,{tag: 'tab-b'});
      }
    }*/
  }

  public setRandomInt(){
    this.randomInt = [];
    for(let i=0; i<100; i++){
      var tempArr = [];
      for(let j=0; j<10; j++){
        tempArr.push(Math.floor(Math.random() * 9 + 10)+Math.random().toFixed(2));
      }
      this.randomInt.push(tempArr);
    }
  }

  public removeCacheTag(tag){
   /* this._ngCache.removeTag(tag);
    this.setRandomInt();
    this._ngCache.set(tag,this.randomInt,{tag:tag});*/
  }

}
