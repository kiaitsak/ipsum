import {Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map, filter, scan } from 'rxjs/operators';
//import {JsonDataService} from '../../services/json-data.service'

@Component({
  selector: 'app-tab-c',
  templateUrl: './tab-c.component.html',
  styleUrls: ['./tab-c.component.css'],
  //providers: [JsonDataService]
})
export class TabCComponent implements OnInit {
  jsonData = Array();
  constructor(private http: Http) { }

  ngOnInit() {
    this.getJson();
  }

  public getJson(){
    let url = 'https://jsonplaceholder.typicode.com/posts';
    //let url = 'assets/MOCK_DATA_2.json';
    this.callJson(url).subscribe(data => {
      this.jsonData = data.json();
	  this.jsonData = Array.of(this.jsonData); 
	  this.jsonData = this.jsonData[0];
    },
	  err => console.error(err), 
	  () => console.log(this.jsonData) 
	  );
  }

    public callJson(url : string) {
        // Returns response
        return this.http.get(url);
    }
}
