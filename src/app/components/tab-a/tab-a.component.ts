import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-a',
  templateUrl: './tab-a.component.html',
  styleUrls: ['./tab-a.component.css']
})
export class TabAComponent implements OnInit {
  colHead = [];
  randomInt = [];

  constructor() { }

  ngOnInit() {
    this.colHead = [1,2,3,4,5,6,7,8,9,10];

    for(let i=0; i<100; i++){
      var tempArr = [];
      for(let j=0; j<10; j++){
        tempArr.push(Math.floor(Math.random() * 9 + 10)+Math.random().toFixed(2));
      }
      this.randomInt.push(tempArr);
    }
  }

}
