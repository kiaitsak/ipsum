import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topmenu',
  templateUrl: './topmenu.component.html',
  styleUrls: ['./topmenu.component.css']
})
export class TopmenuComponent implements OnInit {
  topMenu;
  constructor() { }

  ngOnInit() {
    this.topMenu = [
      {"label":"Home","link":"/home"},
      {"label":"Tab A","link":"/tabA"},
      {"label":"Tab B","link":"/tabB"},
      {"label":"Tab C","link":"/tabC"},
      {"label":"Tab D","link":"/tabD"},
    ];
  }

}
