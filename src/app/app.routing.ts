import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { TabAComponent } from './components/tab-a/tab-a.component';
import { TabBComponent } from './components/tab-b/tab-b.component';
import { TabCComponent } from './components/tab-c/tab-c.component';
import { TabDComponent } from './components/tab-d/tab-d.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'tabA', component: TabAComponent },
    { path: 'tabB', component: TabBComponent },
    { path: 'tabC', component: TabCComponent },
    { path: 'tabD', component: TabDComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);